package org.abbtech.annotaiontask;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LoggableAspect {

    @Pointcut("@annotation(org.abbtech.annotaiontask.Loggable)")
    public void loggableMethod() {}

    @Around("loggableMethod() && @annotation(loggable)")
    public Object logExecution(ProceedingJoinPoint joinPoint, Loggable loggable) throws Throwable {
        System.out.println("Priority: " + loggable.value());
        return joinPoint.proceed();
    }
}

