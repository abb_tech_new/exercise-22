package org.abbtech.annotaiontask;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class SecuredAspect {

    @Pointcut("@annotation(org.abbtech.annotaiontask.Secured)")
    public void securedMethod() {}

    @Around("securedMethod() && @annotation(secured) && args(role)")
    public Object checkSecurity(ProceedingJoinPoint joinPoint, Secured secured, Secured.Role role) throws Throwable {
        if (secured.value() == role) {
            System.out.println("Authorized");
        } else {
            System.out.println("Not Authorized");
        }
        return joinPoint.proceed();
    }
}

