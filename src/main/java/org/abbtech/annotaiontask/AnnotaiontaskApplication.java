package org.abbtech.annotaiontask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AnnotaiontaskApplication {

	public static void main(String[] args) {
		SpringApplication.run(AnnotaiontaskApplication.class, args);
	}

}
