package org.abbtech.annotaiontask;

public class TestClass {

    @Secured(Secured.Role.ADMIN)
    public void securedMethod(Secured.Role role) {
        System.out.println("Inside securedMethod");
    }

    @Loggable(Loggable.Priority.HIGH)
    public void unsecuredMethod() {
        System.out.println("Inside loggable");
    }

    public static void main(String[] args) {
        TestClass testClass = new TestClass();

        testClass.securedMethod(Secured.Role.ADMIN);
        testClass.securedMethod(Secured.Role.USER);
        testClass.unsecuredMethod();
    }
}
